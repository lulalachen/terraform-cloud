output "public_ip" {
  value = module.server.public_ip
}

output "public_dns" {
  value = module.server.public_dns
}

output "iam_policy" {
  value = module.template_file.iam_policy
}
