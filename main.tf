terraform {
  required_version = ">= 0.12.0"
  backend "remote" {
    organization = "lulalachen"

    workspaces {
      name = "terraform-cloud"
    }
  }
}

provider "aws" {
  version = ">= 2.27.0"
}

module "server" {
  source = "./server"

  num_webs     = var.num_webs
  identity     = var.identity
  ami          = var.ami
  ingress_cidr = var.ingress_cidr
  public_key   = var.public_key
  private_key  = var.private_key
}


module "template_file" {
  source   = "./04_template_file"
  owner_id = "tf201c-hashiconf-dove"
}

module "multi_provider" {
  source       = "./05_multi-provider"
  identity     = var.identity
  github_token = var.github_token
  ami          = var.ami
  public_key   = var.public_key
  private_key  = var.private_key
}

module "lifecycle" {
  source = "./06_lifecycles"
}

module "animal" {
  source  = "app.terraform.io/lulalachen/animal/demo"
  version = "1.0.2"
  name    = "data.terraform_remote_state.lulalachen_terraform_cloud.public_dns"
}
